import utils.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Basket<Product> basket = new Basket<>();
        for (; ; ) {
            String inputString = scanner.nextLine();

            if (inputString.equals("EXIT")) {
                break;
            }

            if (inputString.equals("CANCEL")) {
                basket.pop();
                continue;
            }
            if (inputString.equals("PRINT")) {
                double sum = 0;
                for (Product product : basket.reverseBasket()) {
                    System.out.println(product.getName() + ": " + product.getCost());
                    sum += product.getCost();
                }
                System.out.println("TOTAL: " + sum);
                continue;
            }
            try {
                StringValidator stringValidator = new StringValidator(inputString);
                basket.push(new Product(stringValidator.getProductName(), stringValidator.getProductPrice()));
            } catch (IncorrectProductNameException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
