package utils;

import java.io.*;
import java.util.Iterator;

public class Basket<T> implements Iterable<T>, Serializable {
    private final ProductList<T> productList = new ProductList<>();

    public void push(T element) {
        productList.addFirst(element);
    }

    public void pop() {
        productList.removeFirst();
    }

    public Basket<T> reverseBasket() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ObjectOutputStream ous = new ObjectOutputStream(baos)) {
            ous.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        try (ObjectInputStream ois = new ObjectInputStream(bais)) {
            try {
                Basket<T> tempBasket = (Basket<T>) ois.readObject();
                Basket<T> reverseBasket = new Basket<>();
                for (T t : tempBasket) {
                    reverseBasket.push(tempBasket.productList.first.element);
                    tempBasket.pop();
                }
                return reverseBasket;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isEmpty() {
        return productList.isEmpty();
    }

    private class MyIterator implements Iterator<T> {
        private ProductList.Node<T> current = productList.first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            T element = current.element;
            current = current.next;
            return element;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new MyIterator();
    }


    class ProductList<T> implements Serializable {

        private static class Node<T> implements Serializable {
            private T element;
            private Node<T> next;

            public Node(T element) {
                this.element = element;
            }
        }

        private Node<T> first = null;

        public void addFirst(T element) {
            Node<T> newFirst = new Node<>(element);
            newFirst.next = first;
            first = newFirst;
        }

        public void removeFirst() {
            if (first != null) {
                Node<T> oldFirst = first;
                first = first.next;
            }
        }

        public boolean isEmpty() {
            return first == null;
        }

    }
}
