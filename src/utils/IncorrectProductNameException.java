package utils;

public class IncorrectProductNameException extends Exception {
    public IncorrectProductNameException (String message){
        super(message);
    }
}
