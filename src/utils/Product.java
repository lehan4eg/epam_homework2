package utils;

import java.io.Serializable;

public class Product implements Serializable {
    private String name;
    private double cost;

    public Product(String name, double cost) {
        this.cost = cost;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }


}
