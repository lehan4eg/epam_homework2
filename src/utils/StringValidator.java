package utils;

public class StringValidator {


    String productName;
    double productPrice;

    public StringValidator(String string) throws IncorrectProductNameException {
        if (string.contains(":")) {
            String[] strings = string.split(":");
            if (strings.length > 2) {
                throw new IncorrectProductNameException("Incorrect");
            }
            if (strings[0].contains(":") || strings[0].contains("\n")) {
                throw new IncorrectProductNameException("Incorrect");
            }
            if (strings[1].trim().matches("^([0-9]*\\.?[0-9]+)$")) {
                productPrice = Double.parseDouble(strings[1].trim());
            }else {
                productPrice = 0;

            }
            productName = strings[0].trim();
        } else {
            throw new IncorrectProductNameException("Incorrect");
        }
    }

    public String getProductName() {
        return productName;
    }

    public double getProductPrice() {
        return productPrice;
    }
}
